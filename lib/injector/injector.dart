import '../model/award_model.dart';
import '../repository/award_moc_data.dart';
import '../repository/award_prod_data.dart';

import '../model/chat_model.dart';
import '../repository/chat_moc_data.dart';
import '../repository/chat_prod_data.dart';

import '../model/news_model.dart';
import '../repository/news_moc_data.dart';
import '../repository/news_prod_data.dart';

enum ChatFlag { MOCK, PROD }
enum NewsFlag { MOCK, PROD }
enum AwardFlag {MOCK, PROD}

class Injector {
  static final Injector _injector = new Injector._internal();
  static ChatFlag _chat;
  static NewsFlag _news;
  static AwardFlag _award;

  factory Injector() => _injector;
  Injector._internal();

  static void configure({NewsFlag news, ChatFlag chat, AwardFlag award}) {
    _chat = chat;
    _news = news;
    _award = award;
  }

  ChatRepository get chatRepository {
    print("the chat vlaue: $_chat");
    print("the news vlaue: $_news");
    // return new ChatMocData();
    switch (_chat) {
      case ChatFlag.MOCK:
        return new ChatMocData();
        break;
      default:
        return new ChatProdData();
    }
  }

  NewsRepository get newsRepository {
    print("the news vlaue: $_news");
    print('the chat vlaue: $_chat');
    switch (_news) {
      case NewsFlag.MOCK:
        return new NewsMocData();
        break;
      default:
        return new NewsProdData();
    }
  }

  AwardRepository get awardRepository{
    print(_award);
    switch (_award) {
      case AwardFlag.MOCK:
        return new AwardMocData();
        break;
      default:
      return new AwardProdData();
    }
  }
}
