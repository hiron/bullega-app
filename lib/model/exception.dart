class FetchDataException implements Exception{
  final _message;
  FetchDataException([this._message]);

  @override
  String toString(){
    return  _message == null ? super.toString() : _message;
  }

}