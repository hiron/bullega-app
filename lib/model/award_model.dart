import 'dart:async';

class Award {
  final String title;
  final String subtitle;
  final String imageUrl;

  Award({this.title, this.imageUrl, this.subtitle});

  Award.fromMap(Map<String, dynamic> map)
      : this(
          title: map['title'].cast<String>(),
          subtitle: map['subtitle'].cast<String>(),
          imageUrl: map['img'].cast<String>(),
        );
}


abstract class AwardRepository {
  Future<List<Award>> fetchAwardNews();
} 