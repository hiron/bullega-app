import 'dart:async';

class Chat {
  final String msg;
  final String author;

  Chat({this.msg, this.author});

  Chat.fromMap(Map<String, dynamic> map)
      : this(
          msg: map['message'].cast<String>(),
          author: map['author'].cast<String>(),
        );
}

abstract class ChatRepository{
  Future<List<Chat>> fetchChatHistory(); 
}
