import 'dart:async';

class News {
  final int id;
  final String title;
  String subtitle;
  String date;
  String imageUrl;
  String imageBanner;

  News({this.title, this.subtitle, this.date, this.imageUrl, this.id});

  // News.fromMap(Map<String, dynamic> map)
  //     : this(
  //         title: map['title'].cast<String>(),
  //         subtitle: map['body'].cast<String>(),
  //         date: map['date'].cast<String>(),
  //         imageUrl: map['thumbnail'].cast<String>(),
  //       );

  News.fromMap(Map<String, dynamic> map)
      : title = map['title'] as String,
        subtitle = map['body'] as String,
        // imageUrl = map['thumbnail'].cast<String>(),
        // imageBanner = map['image'].cast<String>(),
        imageUrl = null,
        imageBanner = null,
        id = map['id'] as int,
        date = map['date'] as String;
}

abstract class NewsRepository {
  Future<List<News>> fetchNews();
}

abstract class NewsDetailRepository{
  Future<News> fetchDetailNews();
}
