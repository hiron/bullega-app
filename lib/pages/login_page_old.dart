import 'package:flutter/material.dart';

import '../utils/app_colors.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  Widget _loginForm() => new Container(
        padding: const EdgeInsets.symmetric(vertical: 0.0, horizontal: 40.0),
        child: new Form(
          child: new Column(
            children: <Widget>[
              new TextFormField(
                decoration: const InputDecoration(
                  labelText: "Email Address/ User name",
                  labelStyle: TextStyle(
                    color: AppColors.font,
                  ),
                  prefixIcon: Icon(Icons.person, color: AppColors.font),
                  filled: true,
                  fillColor: AppColors.bg,
                  contentPadding: const EdgeInsets.symmetric(
                      vertical: 13.0, horizontal: 25.0),
                  border: const OutlineInputBorder(
                    borderRadius:
                        const BorderRadius.all(const Radius.circular(22.0)),
                  ),
                ),
              ),
              new Padding(
                padding: const EdgeInsets.only(top: 20.0),
              ),
              new TextFormField(
                keyboardType: TextInputType.number,
                obscureText: true,
                decoration: const InputDecoration(
                  labelText: "Password",
                  labelStyle: TextStyle(color: AppColors.font),
                  // icon: Icon(Icons.edit),
                  prefixIcon: const Icon(
                    Icons.lock,
                    color: AppColors.font,
                  ),
                  filled: true,
                  contentPadding: const EdgeInsets.symmetric(
                      vertical: 13.0, horizontal: 25.0),
                  fillColor: AppColors.bg,
                  border: const OutlineInputBorder(
                      borderRadius:
                          const BorderRadius.all(const Radius.circular(22.0))),
                ),
              ),
              new Padding(
                padding: const EdgeInsets.only(top: 40.0),
              ),
              new RaisedButton(
                padding: const EdgeInsets.symmetric(
                    vertical: 10.0, horizontal: 145.0),
                child: new Text(
                  "Login",
                  style: new TextStyle(
                    fontSize: 15.0,
                    color: AppColors.fontLight,
                  ),
                ),
                color: AppColors.font,
                shape: new OutlineInputBorder(
                    borderRadius:
                        const BorderRadius.all(const Radius.circular(20.0))),
                onPressed: () {},
              ),
            ],
          ),
        ),
      );

  Widget _body() => new Column(
        children: <Widget>[
          new Container(
            padding: const EdgeInsets.only(top: 40.0, left: 90.0, right: 90.0),
            child: new Image.asset(
              "images/login_logo.png",
              fit: BoxFit.contain,
            ),
          ),
          new Container(
            padding: EdgeInsets.all(40.0),
            child: new Text(
              "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi beatae officia explicabo! Harum quis dolorum quam quibusdam aliquid earum ratione amet eveniet odio exercitationem quia laboriosam laudantium, enim ullam commodi.",
              style: new TextStyle(
                color: AppColors.font,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          _loginForm(),
          new Container(
            padding: EdgeInsets.only(top: 40.0),
            child: new Row(
              // crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Text(
                  "Do not have an Account?",
                  style: TextStyle(color: AppColors.font),
                ),
                new FlatButton(
                  onPressed: () {},
                  // color: Colors.red,
                  child: Text(
                    "Register",
                    style: TextStyle(color: AppColors.buttonPrimary),
                  ),
                )
              ],
            ),
          ),
        ],
      );

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    return new Scaffold(
      appBar: new AppBar(
        centerTitle: true,
        title: new Text(
          'Login',
          style: new TextStyle(
            color: AppColors.fontLight,
          ),
          textAlign: TextAlign.center,
        ),
        leading: new IconButton(
          icon: new Icon(
            Icons.arrow_back_ios,
            color: AppColors.bg,
          ),
          onPressed: () {},
        ),
        actions: <Widget>[
          new FlatButton(
            child: new Text(
              "Skip",
              style: new TextStyle(
                color: AppColors.fontLight,
              ),
            ),
            onPressed: () {},
          )
        ],
      ),
      body: new Stack(
        overflow: Overflow.visible,
        fit: StackFit.expand,
        children: <Widget>[
          new Image(
            image: new AssetImage("images/slide_bg.png"),
            fit: BoxFit.cover,
          ),
          new AnimatedContainer(
            padding: mediaQuery.padding,
            curve: Curves.ease,
            duration: Duration(milliseconds: 300),
            child: new LayoutBuilder(
              builder: (BuildContext context, BoxConstraints constrains) {
                // print(constrains);
                if (constrains.maxHeight > 10) {
                  return _body();
                } else {
                  return new Padding(
                    padding: EdgeInsets.only(top: 20.0),
                    child: _loginForm(),
                  );
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}
