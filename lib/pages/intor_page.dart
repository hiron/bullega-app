import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../utils/app_colors.dart';
import '../utils/app_navigator.dart';
import '../utils/bulega.dart';

class IntroPage extends StatefulWidget {
  @override
  _IntroPageState createState() => new _IntroPageState();
}

class _IntroPageState extends State<IntroPage>
    with SingleTickerProviderStateMixin {
  TabController _controller;
  List _slideItem;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _slideItem = Bulega.slide;
    _controller = new TabController(vsync: this, length: _slideItem.length);
    SharedPreferences.getInstance().then((value) {
      // print(value.getBool('intro'));
      value.setBool("intro", true);
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _controller.dispose();
    super.dispose();
  }

  Widget _slidView({String img, String title, String text}) {
    return new Container(
      padding: const EdgeInsets.only(top: 50.0),
      alignment: Alignment.center,
      child: new Center(
        child: new Column(
          children: <Widget>[
            new Expanded(
              flex: 2,
              child: new Text(
                title,
                style: new TextStyle(
                    fontSize: 50.0,
                    fontWeight: FontWeight.bold,
                    color: AppColors.font),
                textAlign: TextAlign.center,
              ),
            ),
            new Expanded(
              flex: 3,
              child: new Image.asset(
                img,
                // height: 250.0,
                fit: BoxFit.contain,
                // scale: 3.0,
              ),
            ),
            new Padding(
              padding: const EdgeInsets.all(10.0),
            ),
            new Expanded(
              flex: 3,
              child: new Container(
                padding: const EdgeInsets.all(50.0),
                child: new Text(
                  text,
                  style: new TextStyle(
                    fontSize: 14.0,
                    color: AppColors.font,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Stack(
        fit: StackFit.expand,
        children: <Widget>[
          new Image(
            image: new AssetImage("images/slide_bg.png"),
            fit: BoxFit.cover,
          ),
          new Column(
            // mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              new Expanded(
                flex: 9,
                child: new TabBarView(
                  controller: _controller,
                  children: _slideItem
                      .map((data) => _slidView(
                          title: data['title'],
                          img: data['image'],
                          text: data['text']))
                      .toList(),
                ),
              ),
              new Expanded(
                flex: 1,
                child: new Container(
                  // height: 48.0,
                  alignment: Alignment.center,
                  child: new TabPageSelector(
                    controller: _controller,
                    color: AppColors.font,
                    indicatorSize: 10.0,
                  ),
                ),
              ),
              new Expanded(
                flex: 1,
                child: new Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    new Expanded(
                      flex: 1,
                      child: new RaisedButton(
                        child: new Text("SKIP",
                            style: new TextStyle(
                                fontFamily: "Gotham", fontSize: 15.0)),
                        textColor: AppColors.bg,
                        color: AppColors.buttonPrimary,
                        elevation: 0.0,
                        padding: const EdgeInsets.symmetric(
                            vertical: 20.0, horizontal: 0.0),
                        onPressed: () {
                          AppNavigator.goToLogin(context);
                        },
                      ),
                    ),
                    new Expanded(
                      flex: 1,
                      child: new RaisedButton(
                        child: new Text("NEXT",
                            style: new TextStyle(
                                fontFamily: "Gotham", fontSize: 15.0)),
                        textColor: AppColors.bg,
                        color: AppColors.buttonSecondary,
                        elevation: 0.0,
                        padding: const EdgeInsets.symmetric(
                            vertical: 20.0, horizontal: 0.0),
                        onPressed: () {
                          int nextIndex = _controller.index + 1;

                          nextIndex >= _controller.length
                              ? AppNavigator.goToLogin(context)
                              : _controller.animateTo(nextIndex);
                        },
                      ),
                    ),
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
