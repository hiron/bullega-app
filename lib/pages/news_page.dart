import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import '../model/news_model.dart';
import '../modules/news_presenter.dart';
import '../utils/app_colors.dart';
import '../utils/app_navigator.dart';
import '../utils/app_widgets.dart';

class NewsPage extends StatefulWidget {
  @override
  _NewsPageState createState() => new _NewsPageState();
}

class _NewsPageState extends State<NewsPage> implements NewsListContracts {
  NewsListPresenter _presenter;

  _NewsPageState() {
    // _presenter = new NewsListPresenter(this);
  }

  @override
  void initState() {
    super.initState();
    _presenter = new NewsListPresenter(this);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppWidgets().appBar(
        title: "News",
        leadinaText: "Home",
        automaticallyImplyAction: false,
        leadOnPressed: () {
          Navigator.of(context).pop();
        },
      ),
      body: new Stack(
        fit: StackFit.expand,
        children: <Widget>[
          new Image(
            image: const AssetImage("images/bg.png"),
            fit: BoxFit.cover,
          ),
          new Container(
            padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 25.0),
            child: new Column(
              children: <Widget>[
                AppWidgets().textField(
                    hintText: "Search news....",
                    suffixIcon: new Icon(
                      Icons.search,
                      color: Colors.red,
                    ),
                    onFieldSubmitted: (String Value) {}),
                new Flexible(
                  child: new FutureBuilder(
                    future: _presenter.loadNews(),
                    builder: (BuildContext context, AsyncSnapshot snapshot) {
                      if (snapshot.hasData) {
                        List news = snapshot.data;
                        // print(news.length);
                        return new ListView.builder(
                          itemCount: news == null ? 0 : news.length,
                          itemBuilder: (BuildContext context, int index) {
                            final News data = news[index];
                            // print(data.title);
                            return new NewsListTile(
                              data: data,
                            );
                          },
                        );
                        // return Container();
                      } else {
                        return new Container(
                          child: new Center(
                            child: new CircularProgressIndicator(),
                          ),
                        );
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  Exception onLoadNewsError(error) {
    // TODO: implement onLoadNewsError
    print("the error is: ${error.toString()}");
  }

  @override
  Future onNewsDataLoad(d) async {
    await d.forEach((value) {
      String str = value.subtitle;
      DateTime date = DateTime.parse(value.date);
      value.date = "${date.day} ${monthFormater(date.month)} ${date.year}";
      int endIndex = str.length <= 100 ? str.length : 100;
      value.subtitle = str.substring(0, endIndex);
      value.imageUrl = value.imageUrl == null || value.imageUrl.trim() == ""
          ? "images/news_pic_demo.png"
          : value.imageUrl;
    });
    return Future.value(d);
  }

  String monthFormater(int month) {
    switch (month) {
      case 1:
        return "Jan";
        break;
      case 2:
        return "Feb";
        break;
      case 3:
        return "Mar";
        break;
      case 4:
        return "Apr";
        break;
      case 5:
        return "May";
        break;
      case 6:
        return "Jun";
        break;
      case 7:
        return "July";
        break;
      case 8:
        return "Aug";
        break;
      case 9:
        return "Sep";
        break;
      case 10:
        return "Oct";
        break;
      case 11:
        return "Nov";
        break;
      case 12:
        return "Dec";
        break;
    }
  }
}

class NewsListTile extends StatelessWidget {
  final News data;
  NewsListTile({this.data, Key key}) : super(key: key);

  Widget _subtitle(String subtitle, String date) {
    TextSpan sub = new TextSpan(
      text: subtitle + "\n\n",
      style: new TextStyle(
        fontSize: 12.0,
        color: AppColors.font,
      ),
    );
    TextSpan showDate = new TextSpan(
      text: date,
      // style: new TextStyle(fontSize: 10.0),
      style: new TextStyle(fontSize: 12.0, color: AppColors.font),
    );
    return new RichText(
      text: new TextSpan(children: [sub, showDate]),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new GestureDetector(
      onTap: () {
        AppNavigator.goToNewsDetail(context, data.id);
        
      },
      child: new Column(
        children: <Widget>[
          new Row(
            children: <Widget>[
              new Container(
                width: 100.0,
                height: 100.0,
                decoration: new BoxDecoration(
                  color: Colors.red,
                  image: new DecorationImage(
                    image: new AssetImage(data.imageUrl),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              new Flexible(
                child: new Container(
                  padding: const EdgeInsets.only(left: 20.0),
                  child: new Column(
                    // mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text(
                        data.title.trim(),
                        textAlign: TextAlign.left,
                        style: new TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      new Padding(padding: const EdgeInsets.only(bottom: 10.0)),
                      _subtitle(data.subtitle, data.date),
                    ],
                  ),
                ),
              ),
            ],
          ),
          new Divider()
        ],
      ),
    );
  }
}
