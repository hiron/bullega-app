import 'package:flutter/material.dart';

import '../utils/app_colors.dart';
import '../utils/app_navigator.dart';
import '../utils/app_widgets.dart';

class RegistrationPage4 extends StatefulWidget {
  @override
  _RegistrationPage4State createState() => new _RegistrationPage4State();
}

class _RegistrationPage4State extends State<RegistrationPage4> {
  bool _value = false;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppWidgets().appBar(
        title: "Register",
        leadinaText: "Back",
        leadOnPressed: () {
          Navigator.pop(context);
        },
        actionOnPressed: () {
          AppNavigator.goToHome(context);
        },
      ),
      body: new Stack(
        overflow: Overflow.clip,
        fit: StackFit.expand,
        children: <Widget>[
          const Image(
            image: const AssetImage('images/slide_bg.png'),
            fit: BoxFit.cover,
          ),
          new SingleChildScrollView(
            child: new Column(
              children: <Widget>[
                new Container(
                  padding:
                      const EdgeInsets.only(top: 30.0, left: 90.0, right: 90.0),
                  child: new Image.asset(
                    "images/login_logo.png",
                    fit: BoxFit.contain,
                  ),
                ),
                new Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: 40.0, vertical: 30.0),
                  child: new Column(
                    children: <Widget>[
                      new RichText(
                        textAlign: TextAlign.center,
                        text: new TextSpan(
                            style: new TextStyle(
                              color: AppColors.font,
                            ),
                            children: <TextSpan>[
                              new TextSpan(
                                text:
                                    "For our regular member you have to pay \n",
                              ),
                              new TextSpan(
                                  text: "\$60",
                                  style: const TextStyle(
                                    color: Colors.red,
                                    fontWeight: FontWeight.bold,
                                  )),
                              new TextSpan(
                                text: " to get connected with\n",
                              ),
                              new TextSpan(
                                text: "Nicolo Bulega",
                                style: new TextStyle(
                                  color: AppColors.font,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18.0,
                                ),
                              )
                            ]),
                      ),
                    ],
                  ),
                ),
                new Container(
                  padding: const EdgeInsets.symmetric(
                      vertical: 30.0, horizontal: 30.0),
                  child: new Form(
                    child: new Column(
                      // mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        new Padding(
                          padding: EdgeInsets.only(top: 20.0),
                        ),
                        new Row(
                          children: <Widget>[
                            new Expanded(
                              child: AppWidgets().submitButton(
                                  buttonWidget: new Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: <Widget>[
                                      new Image(
                                        height: 22.0,
                                        image: new AssetImage(
                                          "images/reg_paypal.png",
                                        ),
                                      ),
                                      new Text(
                                        "Pay with paypal",
                                        style: const TextStyle(
                                            color: AppColors.font),
                                      ),
                                      new Icon(
                                        Icons.arrow_forward_ios,
                                        color: AppColors.font,
                                      ),
                                    ],
                                  ),
                                  buttonColor: AppColors.bg,
                                  onpressed: () {}),
                            ),
                          ],
                        ),
                        new Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              new Flexible(
                                child: new Container(
                                  height: 18.0,
                                  // height: 20.0,
                                  decoration: new BoxDecoration(
                                    // color: Colors.blue,
                                    // shape: BoxShape.circle,

                                    image: new DecorationImage(
                                        image: _value
                                            ? new AssetImage(
                                                "images/reg_tic_cir.png")
                                            : new AssetImage(
                                                "images/reg_cir.png"),
                                        fit: BoxFit.contain),
                                  ),
                                  child: new Opacity(
                                    opacity: 0.0,
                                    child: new Checkbox(
                                      value: _value,
                                      tristate: false,
                                      onChanged: (bool value) {
                                        setState(() {
                                          _value = value;
                                        });
                                      },
                                    ),
                                  ),
                                ),
                              ),
                              new Flexible(child: new Text("Accept all")),
                              new Flexible(
                                flex: 2,
                                child: new FlatButton(
                                  child: new Text(
                                    "Tearm and Condition",
                                  ),
                                  textColor: Colors.red,
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 0.0, horizontal: 4.0),
                                  onPressed: () {},
                                ),
                              )
                            ],
                          ),
                        ),
                        new Row(
                          children: <Widget>[
                            new Expanded(
                              child: AppWidgets().submitButton(
                                text: "Enjoy",
                                onpressed: _value
                                    ? () {
                                        AppNavigator.goToLogin(context);
                                      }
                                    : null,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
