import 'package:bulega_app/utils/app_colors.dart';
import 'package:bulega_app/utils/app_navigator.dart';
import 'package:bulega_app/utils/app_widgets.dart';
import "package:flutter/material.dart";

class JoinPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppWidgets().appBar(
          title: "Join Club",
          automaticallyImplyAction: false,
          leadinaText: "Home",
          leadOnPressed: () {
            AppNavigator.goToHome(context);
          }),
      body: new Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Image(
            image: AssetImage("images/bg.png"),
            fit: BoxFit.cover,
          ),
          SingleChildScrollView(
            child: Container(
              margin: EdgeInsets.only(top: 90.0, left: 40.0, right: 40.0),
              child: Column(
                children: <Widget>[
                  Image(
                    fit: BoxFit.fitHeight,
                    height: 110.0,
                    image: AssetImage("images/home_logo.png"),
                  ),
                  Container(
                    padding: EdgeInsets.all(50.0),
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(children: [
                        TextSpan(
                          text:
                              "You are not our club member. If you are interested you can join our Fan Club and talk with \n",
                          style: TextStyle(
                            color: AppColors.font,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        TextSpan(
                            text: "Nicolo Bulega.",
                            style: TextStyle(
                                color: Colors.red,
                                fontSize: 20.0,
                                fontWeight: FontWeight.bold))
                      ]),
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.all(70.0),
                          child: AppWidgets().submitButton(
                              text: "Register",
                              onpressed: () {
                                AppNavigator.goToRegister(context);
                              }),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
