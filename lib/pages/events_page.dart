import 'package:bulega_app/utils/app_colors.dart';
import 'package:flutter/material.dart';

class EventsPage extends StatefulWidget {
  @override
  _EventsPageState createState() => new _EventsPageState();
}

class _EventsPageState extends State<EventsPage>
    with TickerProviderStateMixin  {
  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        new Image(
          image: const AssetImage("images/bg.png"),
          fit: BoxFit.cover,
        ),
        new CustomScrollView(
          slivers: <Widget>[
            new SliverAppBar(
              elevation: 25.0,
              centerTitle: true,
              snap: true,
              pinned: true,
              floating: true,
              title: new Text(
                "Events & Races",
                style: new TextStyle(
                  color: AppColors.fontLight,
                ),
              ),
              leading: new IconButton(
                icon: new Icon(
                  Icons.arrow_back_ios,
                  color: AppColors.bg,
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              flexibleSpace: new Positioned(
                  child: new FlatButton(
                    child: new Text(
                      "Back",
                      style: const TextStyle(color: AppColors.bg),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                  left: 20.0,
                  top: 35.0),
              /* new Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 0.0),
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new FlatButton(
                        child: new Text(
                          "Back",
                          style: const TextStyle(color: AppColors.bg),
                        ),
                        onPressed:() {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  ),
                ), */
              bottom: new TabBar(
                labelColor: AppColors.bg,
                controller: new TabController(
                  vsync: this,
                  length: 2,
                ),
                tabs: <Widget>[
                  new Tab(
                    text: "Events",
                  ),
                  new Tab(
                    text: "Races",
                  )
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }
}
