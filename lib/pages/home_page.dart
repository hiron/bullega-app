import 'package:flutter/material.dart';

import '../utils/app_colors.dart';
import '../utils/app_navigator.dart';
import '../utils/app_widgets.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Stack(
        fit: StackFit.expand,
        children: <Widget>[
          new Image(
            image: new AssetImage("images/home_bg.png"),
            fit: BoxFit.cover,
          ),
          new SingleChildScrollView(
            child: new Column(
              children: <Widget>[
                new Container(
                  padding: const EdgeInsets.only(
                      top: 30.0, left: 90.0, right: 90.0, bottom: 10.0),
                  child: new Image.asset(
                    "images/login_logo.png",
                    fit: BoxFit.contain,
                  ),
                ),
                new Container(
                  padding: const EdgeInsets.symmetric(
                      vertical: 30.0, horizontal: 45.0),
                  child: new HomePageButton(),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class HomePageButton extends StatefulWidget {
  @override
  _HomePageState createState() => new _HomePageState();
}

class _HomePageState extends State<HomePageButton> {
  Widget _homeButton({
    String leading,
    String title,
    bool trailing: false,
    void onPressed(),
  }) {
    return new AppWidgets().homeButton(
      buttonColor: AppColors.button,
      buttonWidget: new Row(
        children: <Widget>[
          new Padding(
            padding: const EdgeInsets.only(left: 0.0, right: 0.0),
            child: new Container(
              height: 20.0,
              width: 80.0,
              decoration: new BoxDecoration(
                image: new DecorationImage(
                    image: new AssetImage(leading), fit: BoxFit.fitHeight),
              ),
            ),
          ),
          new Text(
            title,
            style: const TextStyle(color: AppColors.bg, fontSize: 15.0),
          ),
          trailing
              ? new Container(
                  child: new Opacity(opacity: 0.0, child: new Text("hiron")),
                  height: 10.0,
                  decoration: const BoxDecoration(
                      shape: BoxShape.circle, color: Colors.red),
                )
              : new Container()
        ],
      ),
      onpressed: onPressed ?? () {},
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        _homeButton(
            title: "Latest News/News",
            leading: "images/home_news.png",
            onPressed: () {
              AppNavigator.goToNews(context);
            }),
        _homeButton(
            title: "Chat",
            leading: "images/home_chat.png",
            trailing: true,
            onPressed: () {
              AppNavigator.goToChat(context);
            }),
        _homeButton(
            title: "Races & Events",
            leading: "images/home_time.png",
            onPressed: () {
              AppNavigator.goToEvents(context);
            }),
        _homeButton(
            title: "Club History",
            leading: "images/home_bike.png",
            onPressed: () {
              AppNavigator.goToHistory(context);
            }),
        _homeButton(
            title: "Award History",
            leading: "images/home_cup.png",
            onPressed: () {
              AppNavigator.goToAward(context);
            }),
        _homeButton(
            title: "Profile",
            leading: "images/home_profile.png",
            onPressed: () {
              AppNavigator.goToProfile(context);
            }),
        _homeButton(
            title: "Join Our Club",
            leading: "images/home_join.png",
            onPressed: () {
              AppNavigator.goToJoin(context);
            }),
      ],
    );
  }
}
