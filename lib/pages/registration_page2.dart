import 'package:flutter/material.dart';

import '../utils/app_colors.dart';
import '../utils/app_navigator.dart';
import '../utils/app_widgets.dart';
import 'registration_page3.dart';

class RegistrationPage2 extends StatefulWidget {
  @override
  _RegistrationPage2State createState() => new _RegistrationPage2State();
}

class _RegistrationPage2State extends State<RegistrationPage2> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppWidgets().appBar(
        title: "Register",
        leadinaText: "Back",
        leadOnPressed: () {
          Navigator.pop(context);
        },
        actionOnPressed: () {
          AppNavigator.goToHome(context);
        },
      ),
      body: new Stack(
        overflow: Overflow.clip,
        fit: StackFit.expand,
        children: <Widget>[
          const Image(
            image: const AssetImage('images/slide_bg.png'),
            fit: BoxFit.cover,
          ),
          new SingleChildScrollView(
            child: new Column(
              children: <Widget>[
                new Container(
                  padding:
                      const EdgeInsets.only(top: 30.0, left: 90.0, right: 90.0),
                  child: new Image.asset(
                    "images/login_logo.png",
                    fit: BoxFit.contain,
                  ),
                ),
                new Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: 40.0, vertical: 30.0),
                  child: new Text(
                    "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi beatae officia explicabo! Harum quis dolorum quam quibusdam aliquid earum ratione amet eveniet odio exercitationem quia laboriosam laudantium, enim ullam commodi.",
                    style: new TextStyle(
                      color: AppColors.font,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                new Container(
                  padding: const EdgeInsets.symmetric(
                      vertical: 30.0, horizontal: 30.0),
                  child: new Form(
                    child: new Column(
                      // mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        AppWidgets().textField(
                            hintText: "Tax code",
                            prefixImage: new ImageIcon(
                              new AssetImage('images/reg_icon.png'),
                              size: 18.0,
                              color: AppColors.font,
                            ),
                            keyboardType: TextInputType.number),
                        AppWidgets().textField(
                            hintText: "Phone No.(+39)",
                            prefixImage: new ImageIcon(
                            new AssetImage('images/reg-phone.png'),
                            size: 18.0,
                            color: AppColors.font,
                          ),
                            keyboardType: TextInputType.number),
                        AppWidgets().textField(
                          hintText: "Email",
                          prefixImage: new ImageIcon(
                            new AssetImage('images/reg_mail.png'),
                            size: 18.0,
                            color: AppColors.font,
                          ),
                        ),
                        new Padding(
                          padding: EdgeInsets.only(top: 20.0),
                        ),
                        new Row(
                          children: <Widget>[
                            new Expanded(
                              child: AppWidgets().submitButton(
                                  text: "Next",
                                  onpressed: () {
                                    Navigator.of(context).push(
                                          new MaterialPageRoute(
                                            builder: (context) =>
                                                new RegistrationPage3(),
                                          ),
                                        );
                                  }),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
