import 'dart:async';
import 'package:flutter/material.dart';

import '../model/chat_model.dart';
import '../modules/chat_presenter.dart';
import '../utils/app_colors.dart';
import '../utils/app_widgets.dart';

class ChatPage extends StatefulWidget {
  @override
  _ChatPageState createState() => new _ChatPageState();
}

class _ChatPageState extends State<ChatPage> implements ChatHistoryContracts {
  ChatHistoryPresenter _chatPresenter;
  final List<ChatMessage> _chatHistory = <ChatMessage>[];
  final TextEditingController _textController = new TextEditingController();
  final bool _isOnline = true;

  _ChatPageState() {
    _chatPresenter = new ChatHistoryPresenter(this);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      _chatPresenter.loadChatHistory();
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppWidgets().appBar(
        title: "Chat",
        leadinaText: "Back",
        automaticallyImplyAction: false,
        leadOnPressed: () {
          Navigator.of(context).pop();
        },
      ),
      body: new Stack(
        fit: StackFit.expand,
        children: <Widget>[
          new Image(
            image: const AssetImage("images/bg.png"),
            fit: BoxFit.cover,
          ),
          new Container(
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: new Column(
              children: <Widget>[
                new Container(
                  height: 30.0,
                  padding: const EdgeInsets.symmetric(
                      horizontal: 5.0, vertical: 5.0),
                  decoration: new BoxDecoration(
                    color: AppColors.alertBg,
                    shape: BoxShape.rectangle,
                    borderRadius: const BorderRadius.only(
                      bottomLeft: Radius.circular(20.0),
                      bottomRight: Radius.circular(20.0),
                    ),
                  ),
                  constraints: const BoxConstraints(minWidth: double.maxFinite),
                  child: new Column(
                    children: <Widget>[
                      new Flexible(
                        child: _isOnline
                            ? new Text(
                                "Nicolo Bulega is oneline is for more then 2 hours",
                                style: new TextStyle(
                                  color: AppColors.bg,
                                  fontWeight: FontWeight.bold,
                                ),
                                textAlign: TextAlign.center,
                              )
                            : new Text(
                                "Now Nicolo Bulega is offline.",
                                style: new TextStyle(
                                  color: AppColors.bg,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                      ),
                    ],
                  ),
                ),
                new Flexible(
                  child: _isOnline
                      ? _chatHistory.isEmpty
                          ? new Container(
                              child: new Center(
                                child: new CircularProgressIndicator(),
                              ),
                            )
                          : new ListView.builder(
                              reverse: true,
                              itemCount: _chatHistory.length,
                              itemBuilder: (_, index) {
                                return _chatHistory[index];
                                //new ChatMessage(chat: message);
                              },
                            )
                      : new WaitingScreen(), /* new FutureBuilder(
                    future: _chatPresenter.loadChatHistory(),
                    builder: (BuildContext cotext, AsyncSnapshot snapshot) {
                      if (snapshot.hasError) {
                        return new Container(
                            child: new Center(
                                child: new CircularProgressIndicator(),
                                ),
                            );
                      } else {
                        List<ChatMessage> chatDataList = snapshot.data;
                        return ListView.builder(
                          reverse: true,
                          itemCount: chatDataList.length,
                          itemBuilder: (_, index) {
                            return chatDataList[index];
                            //new ChatMessage(chat: message);
                          },
                        );
                      }
                    },
                  ), */
                ),
                new Divider(),
                new Container(
                  color: AppColors.bg,
                  child: _textComposeWidget(),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _textComposeWidget() {
    return Row(
      children: <Widget>[
        new Flexible(
          child: new TextField(
            enabled: _isOnline,
            decoration: const InputDecoration.collapsed(
              hintText: "Write Your Message...",
            ),
            controller: _textController,
            onSubmitted: _textSubmit,
          ),
        ),
        new IconButton(
          icon: new Icon(Icons.attachment),
          color: AppColors.font,
          onPressed: () {},
        ),
        new IconButton(
          icon: new Icon(Icons.send),
          color: AppColors.font,
          onPressed: () {
            _textSubmit(_textController.text);
          },
        ),
      ],
    );
  }

  _textSubmit(String text) {
    _textController.clear();
    Chat chat = new Chat(author: 'you', msg: text);
    List data = [];
    data.add(chat);
    setState(() {
      _chatHistory.insert(0, new ChatMessage(chat: chat));
    });
  }

  @override
  Exception onLoadChatException(error) {
    // TODO: implement onLoadChatException
    throw new Exception(error);
  }

  @override
  onChatDataLoad(d) async {
    List<Chat> data = await d;
    setState(() {
      data.forEach((chat) {
        _chatHistory.insert(0, new ChatMessage(chat: chat));
      });
    });
  }
}

class ChatMessage extends StatelessWidget {
  final Chat chat;
  ChatMessage({this.chat});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 20.0),
      child: new Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisAlignment: chat.author.toLowerCase().trim() == 'you'
            ? MainAxisAlignment.end
            : MainAxisAlignment.start,
        children: chat.author.toLowerCase().trim() == 'you'
            ? _yourChatView()
            : _othersChatView(),
      ),
    );
  }

  List<Widget> _othersChatView() {
    return <Widget>[
      new CircleAvatar(
        backgroundColor: AppColors.alertBg,
        backgroundImage: new AssetImage('images/chat_guy.png'),
      ),
      new Flexible(
        child: new Container(
          margin: const EdgeInsets.only(left: 10.0, right: 50.0),
          padding: const EdgeInsets.all(10.0),
          decoration: const BoxDecoration(
            color: AppColors.font,
            shape: BoxShape.rectangle,
            borderRadius: const BorderRadius.all(Radius.circular(10.0)),
          ),
          constraints: const BoxConstraints(maxHeight: double.maxFinite),
          child: new Text(
            chat.msg,
            style: const TextStyle(color: AppColors.bg),
          ),
        ),
      )
    ];
  }

  List<Widget> _yourChatView() {
    return <Widget>[
      new Flexible(
        child: new Container(
          margin: const EdgeInsets.only(right: 10.0, left: 50.0),
          padding: const EdgeInsets.all(10.0),
          decoration: const BoxDecoration(
            color: AppColors.buttonPrimary,
            shape: BoxShape.rectangle,
            borderRadius: const BorderRadius.all(Radius.circular(10.0)),
          ),
          constraints: const BoxConstraints(maxHeight: double.maxFinite),
          child: new Text(
            chat.msg,
            style: const TextStyle(color: AppColors.font),
          ),
        ),
      ),
      new CircleAvatar(
        backgroundColor: AppColors.alertBg,
        backgroundImage: new AssetImage('images/chat_man.png'),
      ),
    ];
  }
}

class WaitingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Stack(
      children: <Widget>[
        new Positioned(
          top: 60.0,
          height: 300.0,
          left: 10.0,
          right: 10.0,
          child: const Image(
              image: const AssetImage("images/chat_waiting.png"),
              fit: BoxFit.contain),
        ),
        new Positioned(
          bottom: 70.0,
          left: 40.0,
          right: 40.0,
          child: new Text(
            "Nicolo Bulega is not online now. If he is available then you can chat with him. Thanks for your Patience.",
            textAlign: TextAlign.center,
          ),
        ),

        /* new Container(
          padding: const EdgeInsets.only(top: 60.0, left: 40.0, right: 40.0),
          child: new Column(
            children: <Widget>[
              new Container(
                margin: const EdgeInsets.only(bottom: 20.0),
                width: 200.0,
                height: 300.0,
                decoration: const BoxDecoration(
                    image: const DecorationImage(
                        image: const AssetImage("images/chat_waiting.png"),
                        fit: BoxFit.contain)),
              ),
              new Flexible(
                child: new Text(
                  "Nicolo Bulega is not online now. If he is available then you can chat with him. Thanks for your Patience.",
                  textAlign: TextAlign.center,
                ),
              )
            ],
          ),
        ), */
      ],
    );
  }
}
