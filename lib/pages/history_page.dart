import 'package:bulega_app/utils/app_widgets.dart';
import 'package:bulega_app/utils/app_colors.dart';
import 'package:bulega_app/utils/app_navigator.dart';
import "package:flutter/material.dart";

class HistoryPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppWidgets().appBar(
          title: "Club History",
          leadinaText: "Home",
          automaticallyImplyAction: false,
          leadOnPressed: () {
            Navigator.of(context).pop();
          }),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Image(
            image: AssetImage("images/bg.png"),
            fit: BoxFit.cover,
          ),
          SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 10.0),
              child: Column(
                children: <Widget>[
                  AppWidgets().header(img: AssetImage("images/race1.png")),
                  Container(
                    padding: EdgeInsets.all(10.0),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          flex: 5,
                          child: RichText(
                            text: TextSpan(children: [
                              TextSpan(
                                  text: "Club Name: ",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: AppColors.font)),
                              TextSpan(
                                  text: "Bullegus fan club",
                                  style: TextStyle(color: AppColors.font)),
                            ]),
                          ),
                        ),
                        Expanded(
                          flex: 3,
                          child: RaisedButton(
                            color: Colors.red,
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 2,
                                  child: Icon(
                                    Icons.add,
                                    color: Colors.white,
                                  ),
                                ),
                                Expanded(
                                  flex: 5,
                                  child: Text(
                                    "Join Club",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              ],
                            ),
                            onPressed: () {
                              AppNavigator.goToJoin(context);
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 10.0),
                    alignment: Alignment.topLeft,
                    child: RichText(
                      text: TextSpan(children: [
                        TextSpan(
                          text: "Established: ",
                          style: TextStyle(
                            color: AppColors.font,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        TextSpan(
                          text: "2011",
                          style: TextStyle(color: AppColors.font),
                        )
                      ]),
                    ),
                  ),
                  Container(
                    padding:
                        EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
                    child: Column(
                      children: [
                        Container(
                          alignment: Alignment.topLeft,
                          child: Text(
                            "History: ",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 20.0),
                          child: Text(
                              "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Rerum eaque doloremque, iusto, assumenda quam repellendus aperiam mollitia alias soluta ipsum nemo iste sit reiciendis, expedita vel odit velit exercitationem provident!"),
                        ),
                        Padding(
                          padding: EdgeInsets.all(10.0),
                          child: Image.asset(
                            "images/home_logo.png",
                            height: 80.0,
                            fit: BoxFit.fitHeight,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 20.0),
                          child: Text(
                              "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sed soluta mollitia, suscipit error aperiam recusandae eveniet quam, necessitatibus labore id voluptatem dicta repudiandae aliquid. Ab dolorum doloribus, veritatis, error ratione exercitationem itaque, minima natus ex consequuntur hic asperiores repellat dolore quibusdam animi illo ipsum magni quas adipisci excepturi iste nemo cum. Odio sapiente quos, quaerat veritatis ipsum expedita molestiae, suscipit aliquid culpa tempore nostrum. Fuga impedit est labore quam tempora dolorum eius similique cupiditate facere veniam debitis nobis magnam animi ratione totam aliquid placeat repudiandae, dolor fugiat ullam vel excepturi, sit provident. Consequuntur, quis! Esse consectetur, porro dolore iure numquam iusto nemo magnam exercitationem ducimus laboriosam pariatur. Enim aspernatur, eveniet at, exercitationem doloribus harum assumenda odit iusto, optio id alias provident sint dolorem architecto aliquid autem officia. Aliquid asperiores laboriosam tenetur quas dolorum ex adipisci nostrum, dolores doloribus placeat debitis inventore reiciendis provident veritatis laborum eveniet odio. Commodi optio obcaecati ab minima praesentium quam natus nam nulla dolorem! Perspiciatis odio cupiditate qui, laudantium et reprehenderit dolorum veritatis ipsum id odit quis quo quos illo sint necessitatibus consequatur error doloribus magnam rem iste repellat ipsam! Totam sapiente libero ad qui dolorum, ipsum nisi nemo. Reiciendis molestiae deserunt neque minus, ea rem?"),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
