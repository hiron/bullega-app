import 'package:bulega_app/utils/app_widgets.dart';
import "package:flutter/material.dart";
import "package:share/share.dart";

class NewsDetail extends StatefulWidget {
  final int id;
  NewsDetail(this.id);
  @override
  _NewsDetailState createState() => _NewsDetailState(id);
}

class _NewsDetailState extends State<NewsDetail> {
  final int id;
  _NewsDetailState(this.id);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppWidgets().appBar(
        title: "News Details",
        leadinaText: "Back",
        automaticallyImplyAction: false,
        leadOnPressed: () {
          Navigator.of(context).pop(true);
        },
      ),
      body: NewsDetailBody(
        NewsDetailViewModel(
          body:
              "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sed soluta mollitia, suscipit error aperiam recusandae eveniet quam, necessitatibus labore id voluptatem dicta repudiandae aliquid. Ab dolorum doloribus, veritatis, error ratione exercitationem itaque, minima natus ex consequuntur hic asperiores repellat dolore quibusdam animi illo ipsum magni quas adipisci excepturi iste nemo cum. Odio sapiente quos, quaerat veritatis ipsum expedita molestiae, suscipit aliquid culpa tempore nostrum. Fuga impedit est labore quam tempora dolorum eius similique cupiditate facere veniam debitis nobis magnam animi ratione totam aliquid placeat repudiandae, dolor fugiat ullam vel excepturi, sit provident. Consequuntur, quis! Esse consectetur, porro dolore iure numquam iusto nemo magnam exercitationem ducimus laboriosam pariatur. Enim aspernatur, eveniet at, exercitationem doloribus harum assumenda odit iusto, optio id alias provident sint dolorem architecto aliquid autem officia. Aliquid asperiores laboriosam tenetur quas dolorum ex adipisci nostrum, dolores doloribus placeat debitis inventore reiciendis provident veritatis laborum eveniet odio. Commodi optio obcaecati ab minima praesentium quam natus nam nulla dolorem! Perspiciatis odio cupiditate qui, laudantium et reprehenderit dolorum veritatis ipsum id odit quis quo quos illo sint necessitatibus consequatur error doloribus magnam rem iste repellat ipsam! Totam sapiente libero ad qui dolorum, ipsum nisi nemo. Reiciendis molestiae deserunt neque minus, ea rem?",
          img: "images/award.png",
          title: "Nicholo Bullega the Road Master",
          date: "12 May 2018",
        ),
      ),
    );
  }
}

class NewsDetailBody extends StatelessWidget {
 final NewsDetailViewModel viewModel;

  NewsDetailBody(this.viewModel);

  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        new Image(
          image: const AssetImage("images/bg.png"),
          fit: BoxFit.cover,
        ),
        SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 12.0),
            child: new Column(
              children: <Widget>[
                new AppWidgets().header(
                  img: AssetImage(viewModel.img),
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 5.0),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 5,
                        child: Text(
                          viewModel.title,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Expanded(flex: 1, child: ShareButton()),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  alignment: Alignment.topLeft,
                  child: new Text(viewModel.date),
                ),
                Container(
                  padding: EdgeInsets.only(
                      top: 30.0, left: 10.0, right: 10.0, bottom: 20.0),
                  alignment: Alignment.topLeft,
                  child: new Text(viewModel.body),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class NewsDetailViewModel {
  final String img;
  final String title;
  final String body;
  final String date;

  NewsDetailViewModel({this.img, this.title, this.body, this.date});
}

class ShareButton extends StatefulWidget {
  @override
  _ShareButtonState createState() => _ShareButtonState();
}

class _ShareButtonState extends State<ShareButton> {
  @override
  Widget build(BuildContext context) {
    return new IconButton(
      icon: new Icon(
        Icons.share,
        color: Colors.red,
      ),
      onPressed: () {
        Share.share(
          "Share the Title",
        );
      },
    );
  }
}
