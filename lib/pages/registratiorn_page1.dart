import 'package:flutter/material.dart';

import '../utils/app_colors.dart';
import '../utils/app_navigator.dart';
import '../utils/app_widgets.dart';
import 'registration_page2.dart';

class RegistrationPage1 extends StatefulWidget {
  @override
  _RegistrationPage1State createState() => new _RegistrationPage1State();
}

class _RegistrationPage1State extends State<RegistrationPage1> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppWidgets().appBar(
        title: "Register",
        leadinaText: "Back",
        leadOnPressed: () {
          Navigator.pop(context);
        },
        actionOnPressed: () {
          AppNavigator.goToHome(context);
        },
      ),
      body: new Stack(
        overflow: Overflow.clip,
        fit: StackFit.expand,
        children: <Widget>[
          const Image(
            image: const AssetImage('images/slide_bg.png'),
            fit: BoxFit.cover,
          ),
          new SingleChildScrollView(
            child: new Column(
              children: <Widget>[
                new Container(
                  padding: const EdgeInsets.only(
                      top: 10.0, left: 120.0, right: 120.0),
                  child: new Image.asset(
                    "images/login_logo.png",
                    fit: BoxFit.contain,
                  ),
                ),
                new Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: 40.0, vertical: 30.0),
                  child: new Text(
                    "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi beatae officia explicabo! Harum quis dolorum quam quibusdam aliquid earum ratione amet eveniet odio exercitationem quia laboriosam laudantium, enim ullam commodi.",
                    style: new TextStyle(
                      color: AppColors.font,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                new Container(
                  padding: const EdgeInsets.symmetric(
                      vertical: 30.0, horizontal: 30.0),
                  child: new Form(
                    child: new Column(
                      // mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        AppWidgets().textField(
                          hintText: "Street",
                          prefixImage: new ImageIcon(
                            new AssetImage('images/reg_location.png'),
                            size: 18.0,
                            color: AppColors.font,
                          ),
                        ),
                        AppWidgets().textField(
                          hintText: "Region",
                          prefixImage: new ImageIcon(
                            new AssetImage('images/reg_location.png'),
                            size: 18.0,
                            color: AppColors.font,
                          ),
                        ),
                        AppWidgets().textField(
                          hintText: "Proviens",
                          prefixImage: new ImageIcon(
                            new AssetImage('images/reg_location.png'),
                            size: 18.0,
                            color: AppColors.font,
                          ),
                        ),
                        AppWidgets().textField(
                          hintText: "City",
                          prefixImage: new ImageIcon(
                            new AssetImage('images/reg_location.png'),
                            size: 18.0,
                            color: AppColors.font,
                          ),
                        ),
                        new Padding(
                          padding: EdgeInsets.only(top: 20.0),
                        ),
                        new Row(
                          children: <Widget>[
                            new Expanded(
                              child: AppWidgets().submitButton(
                                  text: "Next",
                                  onpressed: () {
                                    Navigator.of(context).push(
                                          new MaterialPageRoute(
                                              builder: (context) =>
                                                  new RegistrationPage2()),
                                        );
                                  }),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
