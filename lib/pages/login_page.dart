import 'package:flutter/material.dart';

import '../utils/app_colors.dart';
import '../utils/app_navigator.dart';
import '../utils/app_widgets.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    // var mediaQuery = MediaQuery.of(context);
    return new Scaffold(
      appBar: AppWidgets().appBar(
        title: "Login",
        automaticallyImplyLeading: false,
        actionOnPressed: () {
          AppNavigator.goToHome(context);
        },
      ),
      body: new Stack(
        overflow: Overflow.clip,
        fit: StackFit.expand,
        children: <Widget>[
          const Image(
            image: const AssetImage('images/slide_bg.png'),
            fit: BoxFit.cover,
          ),
          new SingleChildScrollView(
            child: new Column(
              children: <Widget>[
                new Container(
                  padding:
                      const EdgeInsets.only(top: 20.0, left: 90.0, right: 90.0),
                  child: new Image.asset(
                    "images/login_logo.png",
                    fit: BoxFit.contain,
                  ),
                ),
                new Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: 40.0, vertical: 30.0),
                  child: new Text(
                    "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi beatae officia explicabo! Harum quis dolorum quam quibusdam aliquid earum ratione amet eveniet odio exercitationem quia laboriosam laudantium, enim ullam commodi.",
                    style: new TextStyle(
                      color: AppColors.font,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                new Container(
                  padding: const EdgeInsets.symmetric(
                      vertical: 30.0, horizontal: 30.0),
                  child: new Form(
                    child: new Column(
                      // mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        AppWidgets().textField(
                          hintText: "Email address/User name",
                          // prefixIcon: Icons.person,
                          prefixImage: new ImageIcon(
                            new AssetImage('images/login_user.png'),
                            size: 15.0,
                            color: AppColors.font,
                          ),
                        ),
                        AppWidgets().textField(
                          hintText: "Password",
                          prefixImage: new ImageIcon(
                            new AssetImage('images/login_pass.png'),
                            size: 18.0,
                            color: AppColors.font,
                          ),
                          obscureText: true,
                        ),
                        new Padding(
                          padding: EdgeInsets.only(top: 20.0),
                        ),
                        new Row(
                          children: <Widget>[
                            new Expanded(
                              child: AppWidgets().submitButton(text: "Login"),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                new Container(
                  padding: EdgeInsets.only(top: 0.0),
                  child: new Row(
                    // crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Text(
                        "Do not have an Account?",
                        style: TextStyle(color: AppColors.font),
                      ),
                      new FlatButton(
                        onPressed: () {
                          AppNavigator.goToRegister(context);
                        },
                        // color: Colors.red,
                        child: Text(
                          "Register",
                          style: TextStyle(color: AppColors.buttonPrimary),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
