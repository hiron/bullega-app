import 'package:flutter/material.dart';

import '../utils/app_colors.dart';
import '../utils/app_navigator.dart';
import '../utils/app_widgets.dart';
import 'registration_page4.dart';

enum Motorcycle { NO, YES }

class RegistrationPage3 extends StatefulWidget {
  @override
  _RegistrationPage3State createState() => new _RegistrationPage3State();
}

class _RegistrationPage3State extends State<RegistrationPage3> {
  Motorcycle _motorcycle = Motorcycle.NO;
  List<String> _cycleTypes = new List();
  String _cycleType;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _cycleTypes.addAll(['12/14y', '14/25y', '34/12y']);
    _cycleType = _cycleTypes.elementAt(0);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppWidgets().appBar(
        title: "Register",
        leadinaText: "Back",
        leadOnPressed: () {
          Navigator.pop(context);
        },
        actionOnPressed: () {
          AppNavigator.goToHome(context);
        },
      ),
      body: new Stack(
        overflow: Overflow.clip,
        fit: StackFit.expand,
        children: <Widget>[
          const Image(
            image: const AssetImage('images/slide_bg.png'),
            fit: BoxFit.cover,
          ),
          new SingleChildScrollView(
            child: new Column(
              children: <Widget>[
                new Container(
                  padding:
                      const EdgeInsets.only(top: 30.0, left: 90.0, right: 90.0),
                  child: new Image.asset(
                    "images/login_logo.png",
                    fit: BoxFit.contain,
                  ),
                ),
                new Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: 40.0, vertical: 30.0),
                  child: new Text(
                    "Do you have any motorcycle?",
                    style: new TextStyle(
                      color: AppColors.font,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                new Container(
                  padding: const EdgeInsets.symmetric(
                      vertical: 30.0, horizontal: 30.0),
                  child: new Form(
                    child: new Column(
                      // crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        new Column(
                          // crossAxisAlignment: CrossAxisAlignment.center,
                          children: Motorcycle.values.map((value) {
                            return new Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                new Radio(
                                  activeColor: AppColors.font,
                                  groupValue: _motorcycle,
                                  value: value,
                                  onChanged: (data) {
                                    setState(() {
                                      _motorcycle = data;
                                    });
                                  },
                                ),
                                new Text(value.toString().split(".")[1]),
                                value == Motorcycle.YES
                                    ? new Padding(
                                        padding:
                                            const EdgeInsets.only(left: 12.0),
                                        child: new Container(
                                            height: 28.0,
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 10.0),
                                            decoration: BoxDecoration(
                                                color: Theme
                                                    .of(context)
                                                    .primaryColor,
                                                borderRadius:
                                                    new BorderRadius.circular(
                                                        22.0)),
                                            child: new DropdownButton(
                                              iconSize: 20.0,
                                              style: const TextStyle(
                                                color: AppColors.bg,
                                              ),
                                              value: _cycleType,
                                              items: _cycleTypes.map((d) {
                                                return new DropdownMenuItem(
                                                  value: d,
                                                  child: new Text(
                                                    d.toString(),
                                                  ),
                                                );
                                              }).toList(),
                                              onChanged: (d) {
                                                setState(() {
                                                  _cycleType = d;
                                                });
                                              },
                                            )),
                                      )
                                    : new Container()
                              ],
                            );
                          }).toList(),
                        ),
                        new Padding(
                          padding: EdgeInsets.only(top: 20.0),
                        ),
                        new Row(
                          children: <Widget>[
                            new Expanded(
                              child: AppWidgets().submitButton(
                                  text: "Next",
                                  onpressed: () {
                                    Navigator.of(context).push(
                                          new MaterialPageRoute(
                                              builder: (context) =>
                                                  new RegistrationPage4()),
                                        );
                                  }),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
