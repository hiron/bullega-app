import 'dart:async';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../utils/app_colors.dart';
import '../utils/app_navigator.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => new _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with WidgetsBindingObserver {
  Future<bool> _checkFirstSeen() {
    return SharedPreferences.getInstance().then((value) {
      return value.getBool('intro') ?? false;
    }).catchError((onError) {
      print(onError);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    Timer(Duration(seconds: 3), () {
      _checkFirstSeen().then((value) {
        if (value)
          AppNavigator.goToHome(context);
        else
          AppNavigator.goToIntro(context);
      });
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
    void didChangeAppLifecycleState(AppLifecycleState state) {
      // TODO: implement didChangeAppLifecycleState
      // super.didChangeAppLifecycleState(state);
      print(state);
    }

  @override
  Widget build(BuildContext context) {
    return new SplashPage();
  }
}

class SplashPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
     return new Scaffold(
      body: new Stack(
        fit: StackFit.expand,
        children: <Widget>[
          new Container(
            decoration: new BoxDecoration(
                color: AppColors.theme,
                image: new DecorationImage(
                    fit: BoxFit.cover,
                    image: new AssetImage("images/splash.png"))),
          ),
          new Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              new CircularProgressIndicator(
                  // backgroundColor: Colors.blue,
                  ),
              new Padding(
                padding: new EdgeInsets.all(80.0),
              )
            ],
          )
        ],
      ),
    );
  }
}