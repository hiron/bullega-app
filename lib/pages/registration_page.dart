import 'package:flutter/material.dart';

import '../utils/app_colors.dart';
import '../utils/app_navigator.dart';
import '../utils/app_widgets.dart';
import 'registratiorn_page1.dart';

class RegistrationPage extends StatefulWidget {
  @override
  _RegistrationPageState createState() => new _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppWidgets().appBar(
        title: "Register",
        leadinaText: "Back",
        leadOnPressed: () {
          Navigator.pop(context);
        },
        actionOnPressed: () {
          AppNavigator.goToHome(context);
        },
      ),
      body: new Stack(
        overflow: Overflow.clip,
        fit: StackFit.expand,
        children: <Widget>[
          const Image(
            image: const AssetImage('images/slide_bg.png'),
            fit: BoxFit.cover,
          ),
          new SingleChildScrollView(
            child: new Column(
              children: <Widget>[
                new Container(
                  padding:
                      const EdgeInsets.only(top: 20.0, left: 90.0, right: 90.0),
                  child: new Image.asset(
                    "images/login_logo.png",
                    fit: BoxFit.contain,
                  ),
                ),
                new Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: 40.0, vertical: 30.0),
                  child: new Text(
                    "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi beatae officia explicabo! Harum quis dolorum quam quibusdam aliquid earum ratione amet eveniet odio exercitationem quia laboriosam laudantium, enim ullam commodi.",
                    style: new TextStyle(
                      color: AppColors.font,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                new Container(
                  padding: const EdgeInsets.symmetric(
                      vertical: 30.0, horizontal: 30.0),
                  child: new Form(
                    child: new Column(
                      // mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        AppWidgets().textField(
                          hintText: "First Name",
                          prefixImage: new ImageIcon(
                            new AssetImage('images/login_user.png'),
                            size: 15.0,
                            color: AppColors.font,
                          ),
                        ),
                        AppWidgets().textField(
                          hintText: "Last Name",
                          prefixImage: new ImageIcon(
                            new AssetImage('images/login_user.png'),
                            size: 15.0,
                            color: AppColors.font,
                          ),
                        ),
                        AppWidgets().textField(
                          hintText: "Birth Date(MM-DD-YYYY)",
                          prefixImage: new ImageIcon(
                            new AssetImage('images/reg_date.png'),
                            size: 16.0,
                            color: AppColors.font,
                          ),
                          keyboardType: TextInputType.number,
                        ),
                        new Padding(
                          padding: EdgeInsets.only(top: 20.0),
                        ),
                        new Row(
                          children: <Widget>[
                            new Expanded(
                              child: AppWidgets().submitButton(
                                  text: "Next",
                                  onpressed: () {
                                    Navigator.of(context).push(
                                        new MaterialPageRoute(
                                            builder: (context) =>
                                                new RegistrationPage1()));
                                  }),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
