import 'package:bulega_app/utils/app_navigator.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

import '../model/award_model.dart';
import '../modules/award_presenter.dart';
import '../utils/app_colors.dart';
import '../utils/app_widgets.dart';

class AwardPage extends StatefulWidget {
  @override
  _AwardPageState createState() => new _AwardPageState();
}

class _AwardPageState extends State<AwardPage> implements AwardNewsContacts {
  AwardNewsPresenter _presenter;
  final List _awardGrides = [];

  _AwardPageState() {
    _presenter = new AwardNewsPresenter(this);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _awardGrides
        .add(new AppWidgets().header(img: new AssetImage("images/award3.png")));
    _presenter.loadAwardNews();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppWidgets().appBar(
        title: "Awards History",
        leadinaText: "Home",
        automaticallyImplyAction: false,
        leadOnPressed: () {
          Navigator.of(context).pop();
        },
      ),
      body: new Stack(
        fit: StackFit.expand,
        children: <Widget>[
          new Image(
            image: const AssetImage("images/bg.png"),
            fit: BoxFit.cover,
          ),
          new Container(
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: new ListView.builder(
              itemCount: _awardGrides.length,
              itemBuilder: (BuildContext context, index) {
                return _awardGrides[index];
              },
            ),
          ),
        ],
      ),
    );
  }

  @override
  void onAwardDataLoad(List d) {
    // TODO: implement onAwardDataLoad
    List<List> formateList(List d) {
      List<List> _list = [];
      for (int index = 0; index < d.length; index += 2) {
        _list.add([d[index], (index + 1) < d.length ? d[index + 1] : null]);
      }
      return _list;
    }

    setState(() {
      _awardGrides.insertAll(
        1,
        formateList(d).map((d) {
          return new Container(
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            height: 250.0,
            child: new Row(
              children: <Widget>[
                new Expanded(
                  child: new AwardTiles(
                    award: d[0],
                  ),
                ),
                new Padding(padding: const EdgeInsets.only(left: 10.0)),
                d[1] == null
                    ? new Expanded(
                        child: new Container(),
                      )
                    : new Expanded(
                        child: new AwardTiles(
                          award: d[1],
                        ),
                      ),
              ],
            ),
          );
        }).toList(),
      );
    });
  }

  @override
  void onLoadAwardException(error) {
    // TODO: implement onLoadAwardException
    print(error);
  }
}

class AwardTiles extends StatelessWidget {
  final Award award;
  final String _sub;

  AwardTiles({@required this.award, Key key})
      : _sub = award.subtitle.substring(
                0, award.subtitle.length <= 50 ? award.subtitle.length : 50) +
            " ...",
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return new GestureDetector(
      onTap: (){
        AppNavigator.goToAwaedDetail(context);
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new Container(
            margin: const EdgeInsets.only(top: 10.0, bottom: 5.0),
            height: 150.0,
            width: double.maxFinite,
            decoration: new BoxDecoration(
              color: AppColors.alertBg,
              shape: BoxShape.rectangle,
              image: new DecorationImage(
                image: new AssetImage(award.imageUrl),
                fit: BoxFit.cover,
              ),
            ),
          ),
          new Text(
            award.title,
            style: const TextStyle(
              fontWeight: FontWeight.bold,
            ),
            textAlign: TextAlign.left,
          ),
          new Flexible(
            child: Padding(
                padding: const EdgeInsets.only(top: 5.0, bottom: 10.0),
                child: new Text(_sub)),
          ),
        ],
      ),
    );
  }
}
