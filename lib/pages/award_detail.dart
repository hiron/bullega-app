import 'package:bulega_app/utils/app_widgets.dart';
import "package:flutter/material.dart";
import "package:share/share.dart";

class AwardDetail extends StatefulWidget {
  @override
  _AwardDetailState createState() => _AwardDetailState();
}

class _AwardDetailState extends State<AwardDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppWidgets().appBar(
        title: "News Details",
        leadinaText: "Back",
        automaticallyImplyAction: false,
        leadOnPressed: () {
          Navigator.of(context).pop();
        },
      ),
      body: AwardDetailBody(
        AwardDetailViewModel(
            body:
                "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sed soluta mollitia, suscipit error aperiam recusandae eveniet quam, necessitatibus labore id voluptatem dicta repudiandae aliquid. Ab dolorum doloribus, veritatis, error ratione exercitationem itaque, minima natus ex consequuntur hic asperiores repellat dolore quibusdam animi illo ipsum magni quas adipisci excepturi iste nemo cum. Odio sapiente quos, quaerat veritatis ipsum expedita molestiae, suscipit aliquid culpa tempore nostrum. Fuga impedit est labore quam tempora dolorum eius similique cupiditate facere veniam debitis nobis magnam animi ratione totam aliquid placeat repudiandae, dolor fugiat ullam vel excepturi, sit provident. Consequuntur, quis! Esse consectetur, porro dolore iure numquam iusto nemo magnam exercitationem ducimus laboriosam pariatur. Enim aspernatur, eveniet at, exercitationem doloribus harum assumenda odit iusto, optio id alias provident sint dolorem architecto aliquid autem officia. Aliquid asperiores laboriosam tenetur quas dolorum ex adipisci nostrum, dolores doloribus placeat debitis inventore reiciendis provident veritatis laborum eveniet odio. Commodi optio obcaecati ab minima praesentium quam natus nam nulla dolorem! Perspiciatis odio cupiditate qui, laudantium et reprehenderit dolorum veritatis ipsum id odit quis quo quos illo sint necessitatibus consequatur error doloribus magnam rem iste repellat ipsam! Totam sapiente libero ad qui dolorum, ipsum nisi nemo. Reiciendis molestiae deserunt neque minus, ea rem?",
            img: "images/award.png",
            title: "Young Star Championship 2015",
            position: "2nd Place",
            points: "239points"),
      ),
    );
  }
}

class AwardDetailBody extends StatelessWidget {
  final AwardDetailViewModel viewModel;

  AwardDetailBody(this.viewModel);

  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        new Image(
          image: const AssetImage("images/bg.png"),
          fit: BoxFit.cover,
        ),
        SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 12.0),
            child: new Column(
              children: <Widget>[
                new AppWidgets().header(
                  img: AssetImage(viewModel.img),
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 5.0),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 5,
                        child: Text(
                          viewModel.title,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Expanded(flex: 1, child: ShareButton()),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                  alignment: Alignment.topLeft,
                  child: new Text(
                    "Position: " + viewModel.position,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  alignment: Alignment.topLeft,
                  child: new Text(
                    "Points: " + viewModel.points,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(
                      top: 30.0, left: 10.0, right: 10.0, bottom: 20.0),
                  alignment: Alignment.topLeft,
                  child: new Text(viewModel.body),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class AwardDetailViewModel {
  final String img;
  final String title;
  final String body;
  final String position;
  final String points;

  AwardDetailViewModel(
      {this.img, this.title, this.body, this.points, this.position});
}

class ShareButton extends StatefulWidget {
  @override
  _ShareButtonState createState() => _ShareButtonState();
}

class _ShareButtonState extends State<ShareButton> {
  @override
  Widget build(BuildContext context) {
    return new IconButton(
      icon: new Icon(
        Icons.share,
        color: Colors.red,
      ),
      onPressed: () {
        Share.share(
          "Share the Title",
        );
      },
    );
  }
}
