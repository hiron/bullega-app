import 'package:bulega_app/utils/app_navigator.dart';
import 'package:bulega_app/utils/app_widgets.dart';
import 'package:flutter/material.dart';

class ProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppWidgets().appBar(
            title: "Profile",
            automaticallyImplyAction: false,
            leadinaText: "Home",
            leadOnPressed: () {
              Navigator.of(context).pop();
            }),
        body: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Image(
              fit: BoxFit.cover,
              image: AssetImage("images/bg.png"),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 50.0, horizontal: 30.0),
              child: Column(
                children: <Widget>[
                  Image(
                    image: AssetImage("images/home_logo.png"),
                    height: 70.0,
                    fit: BoxFit.fitHeight,
                  ),
                  Padding(
                    padding:
                        EdgeInsets.symmetric(vertical: 20.0, horizontal: 70.0),
                    child: Text(
                      "You are not our club member. If you are interested you may login or register",
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 80.0),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: AppWidgets().submitButton(
                              text: "Login",
                              onpressed: () {
                                AppNavigator.goToLogin(context);
                              }),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 30.0),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: AppWidgets().submitButton(
                              text: "Register",
                              onpressed: () {
                                AppNavigator.goToRegister(context);
                              }),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ));
  }
}
