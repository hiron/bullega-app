import 'dart:async';

import '../model/chat_model.dart';

class ChatMocData extends ChatRepository {
  @override
  Future<List<Chat>> fetchChatHistory() {
    // TODO: implement fetchChatHistory
    return Future.value(_data);
  }

  List<Chat> _data = [
    new Chat(msg: "Hi", author: "you"),
    new Chat(msg: "hello", author: "Nicholo Bulega"),
    new Chat(msg: "What are you Doind??", author: "you"),
    new Chat(
        msg:
            "Lorem ipsum dolor sit amet consectetur adipisicing elit.  laboriosam laudantium, enim ullam commodi.",
        author: 'you'),
    new Chat(
        msg:
            "Lorem ipsum dolor sit amet consectetur adipisicing elit.  laboriosam laudantium, enim ullam",
        author: "Nicholo Bulega"),
    new Chat(msg: 'thans and bye', author: 'you'),
    new Chat(msg: 'bye and take care', author: 'Nicholo Bulega')
  ];
}
