import 'dart:async';

import '../model/news_model.dart';

class NewsMocData extends NewsRepository {
  @override
  Future<List<News>> fetchNews() {
    return Future.value(_newsMokData);
  }

  List<News> _newsMokData = <News>[
    new News(
        title: "Nicolo Bullega the Road Master",
        subtitle:
            "Lorem ipsum dolor sit amet consectetur adipisicing elit.  laboriosam laudantium, enim ullam commodi.",
        imageUrl: "images/news_pic.png",
        date: "12 May 2018"),
    new News(
        title: "Nicolo Bullega the Road Master",
        subtitle:
            "Lorem ipsum dolor sit amet consectetur adipisicing elit.  laboriosam laudantium, enim ullam commodi.",
        imageUrl: "images/news_pic_demo.png",
        date: "12 May 2018"),
    new News(
        title: "Nicolo Bullega the Road Master",
        subtitle:
            "Lorem ipsum dolor sit amet consectetur adipisicing elit.  laboriosam laudantium, enim ullam commodi.",
            
        imageUrl: "images/news-pickk.png",
        date: "12 May 2018"),
    new News(
        title: "Nicolo Bullega the Road Master",
        subtitle:
            "Lorem ipsum dolor sit amet consectetur adipisicing elit.  laboriosam laudantium, enim ullam commodi.",
            
        imageUrl: "",
        date: "12 May 2018"),
    new News(
        title: "Nicolo Bullega the Road Master",
        subtitle:
            "Lorem ipsum dolor sit amet consectetur adipisicing elit.  laboriosam laudantium, enim ullam commodi.",
           
        imageUrl: "",
        date: "12 May 2018"),
    new News(
        title: "Nicolo Bullega the Road Master",
        subtitle:
            "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi beatae officia explicabo! Harum quis dolorum quam quibusdam aliquid earum ratione amet eveniet odio exercitationem quia laboriosam laudantium, enim ullam commodi.",
        imageUrl: "",
        date: "12 May 2018"),
    new News(
        title: "Nicolo Bullega the Road Master",
        subtitle:
            "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi beatae officia explicabo! Harum quis dolorum quam quibusdam aliquid earum ratione amet eveniet odio exercitationem quia laboriosam laudantium, enim ullam commodi.",
        imageUrl: "",
        date: "12 May 2018"),
    new News(
        title: "Nicolo Bullega the Road Master",
        subtitle:
            "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi beatae officia explicabo! Harum quis dolorum quam quibusdam aliquid earum ratione amet eveniet odio exercitationem quia laboriosam laudantium, enim ullam commodi.",
        imageUrl: "",
        date: "12 May 2018"),
    new News(
        title: "Nicolo Bullega the Road Master",
        subtitle:
            "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi beatae officia explicabo! Harum quis dolorum quam quibusdam aliquid earum ratione amet eveniet odio exercitationem quia laboriosam laudantium, enim ullam commodi.",
        imageUrl: "",
        date: "12 May 2018"),
    new News(
        title: "Nicolo Bullega the Road Master",
        subtitle:
            "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi beatae officia explicabo! Harum quis dolorum quam quibusdam aliquid earum ratione amet eveniet odio exercitationem quia laboriosam laudantium, enim ullam commodi.",
        imageUrl: "",
        date: "12 May 2018"),
    new News(
        title: "Nicolo Bullega the Road Master",
        subtitle:
            "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi beatae officia explicabo! Harum quis dolorum quam quibusdam aliquid earum ratione amet eveniet odio exercitationem quia laboriosam laudantium, enim ullam commodi.",
        imageUrl: "",
        date: "12 May 2018"),
  ];
}
