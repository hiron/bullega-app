import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

import '../model/exception.dart';
import '../model/news_model.dart';

class NewsProdData extends NewsRepository {
  final Uri url = new Uri.http(
      '192.168.1.108:8888', '/news'); //"http://localhost:8888/news";
  final client = http.Client();
  @override
  Future<List<News>> fetchNews() async {
    // print("I am here ${http.get(url)}");
    return client.get(url).then((response) {
      print(response.statusCode == null);
      return response.statusCode != 200 || response.statusCode == null
          ? throw new FetchDataException(
              "An Error occure:[error Status: ${response.statusCode}]")
          : List<News>.from(jsonDecode(response.body).map((data) {
              // print("the loading data ${data['thumbnail']}");
              // print("the file type: ${News.fromMap(data).runtimeType}");
              return News.fromMap(data);
            }));
    }).catchError((onError) {
      // print("the error thrown from Here: ${onError.toString()}");
      throw new FetchDataException(onError.toString());
    }).whenComplete(client.close);
  }
}
