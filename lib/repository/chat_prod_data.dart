import 'dart:async';
import 'dart:convert';

import '../model/chat_model.dart';
import '../model/exception.dart';
import 'package:http/http.dart' as http;

class ChatProdData extends ChatRepository{
  final String Url = "";
  @override
  Future<List<Chat>> fetchChatHistory() {
    // TODO: implement fetchChatHistory
    return http.get(Url).then((response) {
      return response.statusCode != 200 || response.statusCode == null
          ? throw new FetchDataException(
              "An Error occure:[error Status: ${response.statusCode}]")
          : jsonDecode(response.body)
              .map((data) => new Chat.fromMap(data))
              .toList();
    }).catchError((onError) => throw new FetchDataException("Chat History Error: $onError"));
  }
}
