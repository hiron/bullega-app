import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

import '../model/exception.dart';
import '../model/award_model.dart';

class AwardProdData extends AwardRepository {
  final String url = "127.0.0.1:8888/award";
  @override
  Future<List<Award>> fetchAwardNews() {
    return http.get(url).then((response) {
      return response.statusCode != 200 || response.statusCode == null
          ? throw new FetchDataException(
              "An Error occure:[error Status: ${response.statusCode}]")
          : jsonDecode(response.body)
              .map((data) => new Award.fromMap(data))
              .toList();
    }).catchError((onError) =>
        throw new FetchDataException("Award News Error: $onError"));
  }
}
