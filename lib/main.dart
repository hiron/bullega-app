import 'package:bulega_app/utils/app_colors.dart';
import 'package:bulega_app/utils/app_navigator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'injector/injector.dart';
import 'pages/award_page.dart';
import 'pages/chat_page.dart';
import 'pages/events_page.dart';
import 'pages/history_page.dart';
import 'pages/home_page.dart';
import 'pages/intor_page.dart';
import 'pages/join_page.dart';
import 'pages/login_page.dart';
import 'pages/news_detail.dart';
import 'pages/news_page.dart';
import 'pages/profile_page.dart';
import 'pages/registration_page.dart';
import 'pages/splash_screen.dart';
import 'pages/award_detail.dart';
// import 'utils/app_colors.dart';

void main() {
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
  Injector.configure(
    news: NewsFlag.PROD,
    chat: ChatFlag.MOCK,
    award: AwardFlag.MOCK,
  );
  runApp(new MyApp());
}

/* var routes = <String, WidgetBuilder>{
  '/home': (BuildContext context) => new HomePage(),
  '/intro': (BuildContext context) => new IntroPage(),
  '/login': (BuildContext context) => new LoginPage(),
  '/register': (BuildContext context) => new RegistrationPage(),
  '/news': (BuildContext context) => new NewsPage(),
  '/chat': (BuildContext context) => new ChatPage(),
  '/events': (BuildContext context) => new EventsPage(),
  '/history': (BuildContext context) => new HistoryPage(),
  '/award': (BuildContext context) => new AwardPage(),
  '/profile': (BuildContext context) => new ProfilePage(),
  '/join': (BuildContext context) => new JoinPage(),
  '/detailNews': (BuildContext context) => new NewsDetail(),
}; */

class MyApp extends StatelessWidget {
  Route<dynamic> _routeGenerate(RouteSettings settings) {
    switch (settings.name) {
      case '/home':
        return AppCustomRoute(
            builder: (BuildContext context) => new HomePage(),
            settings: settings);
        break;
      case '/intro':
        return AppCustomRoute(
            builder: (BuildContext context) => new IntroPage(),
            settings: settings);
        break;
      case '/login':
        return AppCustomRoute(
            builder: (BuildContext context) => new LoginPage(),
            settings: settings);
        break;
      case '/register':
        return AppCustomRoute(
            builder: (BuildContext context) => new RegistrationPage(),
            settings: settings);
        break;
      case '/news':
        return AppCustomRoute(
            builder: (BuildContext context) => new NewsPage(),
            settings: settings);
        break;
      // case '/detailNews':
      //   return AppCustomRoute(
      //       builder: (BuildContext context) => new NewsDetail(),
      //       settings: settings);
      //   break;
      case '/chat':
        return AppCustomRoute(
            builder: (BuildContext context) => new ChatPage(),
            settings: settings);
        break;
      case '/events':
        return AppCustomRoute(
            builder: (BuildContext context) => new EventsPage(),
            settings: settings);
        break;
      case '/history':
        return AppCustomRoute(
            builder: (BuildContext context) => new HistoryPage(),
            settings: settings);
        break;
      case '/award':
        return AppCustomRoute(
            builder: (BuildContext context) => new AwardPage(),
            settings: settings);
        break;
      case '/detailAward':
        return AppCustomRoute(
            builder: (BuildContext context) => new AwardDetail(),
            settings: settings);
        break;
      case '/profile':
        return AppCustomRoute(
            builder: (BuildContext context) => new ProfilePage(),
            settings: settings);
        break;
      case '/join':
        return AppCustomRoute(
            builder: (BuildContext context) => new JoinPage(),
            settings: settings);
        break;
      // default:
    }
    // assert(false);
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      theme: new ThemeData(
        primaryColor: AppColors.theme,
        fontFamily: 'Gotham',
        accentColor: AppColors.theme,
        accentIconTheme: IconThemeData(color: AppColors.bg),
        // iconTheme: IconThemeData(color: AppColors.bg),
        canvasColor: AppColors.theme,
      ),
      title: "Bulegars App",
      home: new SplashScreen(),
      onGenerateRoute: _routeGenerate,
      // routes: routes,
      debugShowCheckedModeBanner: false,
    );
  }
}
