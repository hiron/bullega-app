class Bulega {
  static const List<Map<String, String>> slide = [
    {
      "title": "Can See \n Events",
      "image":"images/slide_image.png",
      "text":
          "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi beatae officia explicabo! Harum quis dolorum quam quibusdam aliquid earum ratione amet eveniet odio exercitationem quia laboriosam laudantium, enim ullam commodi."
    },
    {
      "title": "Chat",
      "image":"images/slide_chat.png",
      "text":
          "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi beatae officia explicabo! Harum quis dolorum quam quibusdam aliquid earum ratione amet eveniet odio exercitationem quia laboriosam laudantium, enim ullam commodi."
    },
    {
      "title": "Club\n Members",
      "image":"images/slide_member.png",
      "text":
          "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi beatae officia explicabo! Harum quis dolorum quam quibusdam aliquid earum ratione amet eveniet odio exercitationem quia laboriosam laudantium, enim ullam commodi."
    },
  ];
}
