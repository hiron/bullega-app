import 'package:flutter/material.dart';

import 'app_colors.dart';

class AppWidgets {
  static final AppWidgets _appWidgets = new AppWidgets._internal();

  factory AppWidgets() => _appWidgets;
  AppWidgets._internal();

  /* ---Header Widget--- */
  Widget header({AssetImage img}) {
    return new Container(
      margin: const EdgeInsets.only(bottom: 30.0),
      height: 200.0,
      decoration: new BoxDecoration(
        image: new DecorationImage(image: img, fit: BoxFit.cover),
        color: AppColors.alertBg,
        shape: BoxShape.rectangle,
        borderRadius: const BorderRadius.vertical(
          bottom: Radius.circular(10.0),
        ),
      ),
    );
  }

/* ---- Text Input Field Widget ---- */
  Widget textField(
      {bool obscureText: false,
      String hintText,
      IconData prefixIcon,
      Widget prefixImage,
      Widget suffixIcon,
      void onFieldSubmitted(String value),
      TextInputType keyboardType: TextInputType.text,
      TextEditingController controller}) {
    return new Column(
      children: <Widget>[
        new TextFormField(
          obscureText: obscureText,
          controller: controller,
          onFieldSubmitted: onFieldSubmitted,
          autofocus: false,
          keyboardType: keyboardType,
          decoration: new InputDecoration(
            hintText: hintText ?? "",
            labelStyle: const TextStyle(
              color: AppColors.font,
            ),
            suffixIcon: suffixIcon,
            prefixIcon: prefixIcon == null
                ? prefixImage == null
                    ? null
                    : new Padding(
                        padding: const EdgeInsets.only(left: 10.0),
                        child: prefixImage,
                      )
                : new Padding(
                    padding: const EdgeInsets.only(right: 5.0),
                    child: new Icon(prefixIcon, color: AppColors.font),
                  ),
            filled: true,
            fillColor: AppColors.bg,
            contentPadding:
                const EdgeInsets.symmetric(vertical: 0.0, horizontal: 15.0),
            border: const OutlineInputBorder(
              borderSide: BorderSide(
                // color: Colors.red,
                width: 0.0,
                style: BorderStyle.none,
              ),
              borderRadius: const BorderRadius.all(const Radius.circular(23.0)),
            ),
          ),
        ),
        new Padding(
          padding: EdgeInsets.only(top: 15.0),
        ),
      ],
    );
  }

/* --- AppBar Widget ----- */
  Widget appBar({
    String title,
    IconData leadingIcon: Icons.arrow_back_ios,
    String leadinaText,
    String actionText: "Skip",
    void leadOnPressed(),
    void actionOnPressed(),
    bool automaticallyImplyLeading: true,
    bool automaticallyImplyAction: true,
  }) {
    return new AppBar(
      flexibleSpace: leadinaText == null
          ? null
          : new Padding(
              padding: const EdgeInsets.only(left: 20.0, top: 23.0),
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new FlatButton(
                    child: new Text(
                      leadinaText ?? "",
                      style: const TextStyle(color: AppColors.bg),
                    ),
                    onPressed: leadOnPressed ?? () {},
                  ),
                ],
              ),
            ),
      title: new Text(title ?? "", style: const TextStyle(color: AppColors.bg)),
      centerTitle: true,
      automaticallyImplyLeading: automaticallyImplyLeading,
      leading: !automaticallyImplyLeading
          ? null
          : new IconButton(
              icon: new Icon(
                leadingIcon,
                color: AppColors.bg,
              ),
              onPressed: leadOnPressed ?? () {},
            ),
      actions: !automaticallyImplyAction
          ? null
          : <Widget>[
              new FlatButton(
                child: new Text(
                  actionText,
                  style: const TextStyle(color: AppColors.bg),
                ),
                onPressed: actionOnPressed ?? () {},
              )
            ],
    );
  }

/* ----- Raised Button Widget ----- */
  Widget submitButton(
      {String text, Color buttonColor, Widget buttonWidget, void onpressed()}) {
    return new RaisedButton(
      padding: const EdgeInsets.symmetric(vertical: 13.0),
      child: buttonWidget != null
          ? buttonWidget
          : new Text(
              text,
              style: new TextStyle(
                fontSize: 15.0,
                color: AppColors.fontLight,
              ),
            ),
      color: buttonColor ?? AppColors.font,
      shape: new OutlineInputBorder(
          borderRadius: const BorderRadius.all(const Radius.circular(23.0))),
      onPressed: onpressed ?? null,
    );
  }

/* --- Home Button Widget ---*/
  Widget homeButton({
    String text,
    Color buttonColor,
    Widget buttonWidget,
    void onpressed(),
  }) {
    return new Row(
      children: <Widget>[
        new Expanded(
          child: new Padding(
            padding: const EdgeInsets.only(bottom: 20.0),
            child: new RaisedButton(
              padding: const EdgeInsets.symmetric(vertical: 18.0),
              child: buttonWidget != null
                  ? buttonWidget
                  : new Text(
                      text,
                      style: new TextStyle(
                        fontSize: 15.0,
                        color: AppColors.fontLight,
                      ),
                    ),
              color: buttonColor ?? AppColors.font,
              shape: new OutlineInputBorder(
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(28.0),
                  bottomRight: Radius.circular(28.0),
                ),
              ),
              onPressed: onpressed ?? null,
            ),
          ),
        ),
      ],
    );
  }
}
