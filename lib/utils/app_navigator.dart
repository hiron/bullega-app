import 'package:bulega_app/pages/news_detail.dart';
import 'package:flutter/material.dart';

class AppNavigator {
  static void goToIntro(BuildContext context) =>
      Navigator.pushReplacementNamed(context, "/intro");

  static void goToHome(BuildContext context) =>
      // Navigator.popAndPushNamed(context, "/home" );
      Navigator.pushNamedAndRemoveUntil(context, "/home", (Route<dynamic> route)=>false);

  static void goToLogin(BuildContext context) =>
      Navigator.pushNamed(context, "/login");

  static void goToRegister(BuildContext context) =>
      Navigator.pushNamed(context, "/register");

  static void goToNews(BuildContext context) =>
      Navigator.pushNamed(context, "/news");

  static void goToChat(BuildContext context) =>
      Navigator.pushNamed(context, "/chat");

  static void goToEvents(BuildContext context) =>
      Navigator.pushNamed(context, "/events");

  static void goToHistory(BuildContext context) =>
      Navigator.pushNamed(context, "/history");

  static void goToAward(BuildContext context) =>
      Navigator.pushNamed(context, "/award");

  static void goToProfile(BuildContext context) =>
      Navigator.pushNamed(context, "/profile");

  static void goToJoin(BuildContext context) =>
      Navigator.pushNamed(context, "/join");

  static void goToNewsDetail(BuildContext context, int id) =>
      Navigator.push(context, AppCustomRoute(
        builder:(context)=> NewsDetail(id)
      ));

  static void goToAwaedDetail(BuildContext context) =>
      Navigator.pushNamed(context, "/detailAward");
}

class AppCustomRoute<T> extends MaterialPageRoute<T> {
  AppCustomRoute({WidgetBuilder builder, RouteSettings settings})
      : super(builder: builder, settings: settings);
  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    // TODO: implement buildTransitions
    if (settings.isInitialRoute) {
      return child;
    }
    if (animation.status == AnimationStatus.reverse)
      return super
          .buildTransitions(context, animation, secondaryAnimation, child);

    // print(animation.status);

    return new SlideTransition(
      position: new Tween<Offset>(end: Offset.zero, begin: new Offset(1.0, 0.0))
          .animate(animation),
      child: new FadeTransition(
        opacity: new Tween(
          begin: 1.0,
          end: 0.2
        ).animate(secondaryAnimation),
        child: child,
      ),
    );
  }
}
