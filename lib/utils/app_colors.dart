import 'package:flutter/material.dart';

class AppColors {
static const Color theme = const Color(0xFFfdc012);
static const Color bg = const Color(0xFFFFFFFF);
static const Color button = const Color(0xccfdc012);
static const Color buttonPrimary = const Color(0xfffdc012); 
static const Color buttonSecondary = const Color(0xff8a6829); 
static const Color font = const Color(0xff484748); 
static const Color fontLight = const Color(0xFFFFFFFF);
static const Color alertBg = const Color(0xFFa19e9e);
}