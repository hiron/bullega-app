import 'dart:async';

import '../injector/injector.dart';

import '../model/award_model.dart';

abstract class AwardNewsContacts {
  void onAwardDataLoad(List d);
  void onLoadAwardException(error);
}

class AwardNewsPresenter {
  final AwardNewsContacts _view;
  final AwardRepository _repo;

  AwardNewsPresenter(this._view) : _repo = new Injector().awardRepository;

  Future loadAwardNews() {
    return _repo
        .fetchAwardNews()
        .then((data) => _view.onAwardDataLoad(data))
        .catchError((onError) => _view.onLoadAwardException(onError));
  }
}
