import 'dart:async';

import '../injector/injector.dart';
import '../model/chat_model.dart';

abstract class ChatHistoryContracts {
  void onChatDataLoad(d);
  Exception onLoadChatException(error);
}

class ChatHistoryPresenter {
  ChatHistoryContracts _view;
  final ChatRepository _repository;

  ChatHistoryPresenter(this._view) : _repository = new Injector().chatRepository;

  Future loadChatHistory() {
    return _repository
        .fetchChatHistory()
        .then((d) => _view.onChatDataLoad(d))
        .catchError((onError) => _view.onLoadChatException(onError));
  }
}
