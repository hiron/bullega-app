import 'dart:async';

import '../injector/injector.dart';
import '../model/news_model.dart';

abstract class NewsListContracts {
  Exception onLoadNewsError(error);
  Future onNewsDataLoad(d);
}

class NewsListPresenter {
  NewsListContracts _view;
  NewsRepository _repository;

  NewsListPresenter(this._view) : _repository = new Injector().newsRepository;

  Future loadNews() {
    return _repository.fetchNews().then((d) {
      print("Here I am: ${d.runtimeType}");
      return _view.onNewsDataLoad(d);
    }).catchError((onError) {
      print("Here it is: ${onError.toString()}");
      return _view.onLoadNewsError(onError);
    });
  }
}
